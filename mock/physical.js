import { mock } from "mockjs";

// mock 用户数据
export default [
  // 套餐分类
  {
    url: '/api/physical/classification',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: [
          {
            name: '优雅绅士 男性体检',
            total: 812,
            url: '/physical/packageList?det=man&total=812'
          },
          {
            name: '粉红挚爱 女性体检',
            total: 371,
            url: '/physical/packageList?det=woman&total=372'
          },
          {
            name: '关爱父母 长者体检',
            total: 100,
            url: '/physical/packageList?det=parent&total=100'
          },
          {
            name: '职场无忧 入职体检',
            total: 112,
            url: '/physical/packageList?det=work&total=112'
          }
        ]
      };
    }
  },
  // 套餐列表
  // 根据query不同返回不同的数据
  {
    url: '/api/physical/packageList',
    method: 'get',
    response: (req) => {
      const query = new URLSearchParams(req.url.split('?')[1]); // 获取查询参数
      const det = query.get('det'); // 获取 query.det 的值
      const page = parseInt(query.get('page')) || 1; // 获取页码，默认为第一页
      const pageSize = 20; // 每页显示的条数
      const totalCount = 100

      let responseData;
      switch (det) {
        case 'man':
          responseData = {
            code: 0,
            msg: 'ok',
            data: generateData(totalCount, '鹏程万里体检套餐(男)', '南方医科大学附属第三医院', 1000, 500, page, pageSize)
          };
          break;
          case 'woman':
          responseData = {
            code: 0,
            msg: 'ok',
            data: generateData(totalCount, '花开富贵体检套餐(女)', '东方医科大学附属第三医院', 1200, 700, page, pageSize)
          };
          break;
          case 'parent':
            responseData = {
              code: 0,
              msg: 'ok',
              data: generateData(totalCount, '老年人体检套餐', '北方医科大学附属第三医院', 800, 300, page, pageSize)
            };
            break;
            case 'work':
              responseData = {
                code: 0,
                msg: 'ok',
                data: generateData(totalCount, '入职体检套餐', '西方医科大学附属第三医院', 300, -200, page, pageSize)
          };
          break;
          default:
            responseData = {
              code: -1,
              msg: '请传递正确的det'
            };
          }
          if(page*pageSize > totalCount){
            responseData = {
              code: -1,
              msg: '没数据了'
            };
           }

      

      return responseData;
    }
  },
  // 套餐详情的banner
  {
    url: '/api/physical/packageDetails',
    method: 'get',
    response: (req) => {
      const query = new URLSearchParams(req.url.split('?')[1]); // 获取查询参数
      const det = query.get('det'); 
      const id = parseInt('id')

      let responseData;
      switch (det) {
        case 'man':
          responseData = {
            code: 0,
            msg: 'ok',
            data: {
              id:1,
              bannerurl:'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
              title:'鹏程万里体检套餐(男)',
              location:'南方医科大学附属第三医院',
              oldprice:1498,
              sellingprice:998,
              volume:15941,
              addition:true
            }
          };
          break;
          case 'woman':
            responseData = {
              code: 0,
              msg: 'ok',
              data: {
                id:1,
                bannerurl:'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
                title:'花开富贵体检套餐(女)',
                location:'东方医科大学附属第三医院',
                oldprice:1998,
                sellingprice:1498,
                volume:23751,
                addition:true
              }
          };
          break;
          case 'parent':
            responseData = {
              code: 0,
              msg: 'ok',
              data: {
                id:1,
                bannerurl:'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
                title:'老年人体检套餐',
                location:'北方医科大学附属第三医院',
                oldprice:1298,
                sellingprice:798,
                volume:56413,
                addition:true
              }
            };
            break;
            case 'work':
              responseData = {
                code: 0,
                msg: 'ok',
                data: {
                  id:1,
                  bannerurl:'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
                  title:'入职体检套餐',
                  location:'西方医科大学附属第三医院',
                  oldprice:798,
                  sellingprice:298,
                  volume:35416,
                  addition:false
                }
          };
          break;
          default:
            responseData = {
              code: -1,
              msg: '请传递正确的det'
            };
          }

      

      return responseData;
    }
  },
  // 套餐详情的图文详情
  {
    url: '/api/physical/pictureTextDetails',
    method: 'get',
    response: (req) => {
      const query = new URLSearchParams(req.url.split('?')[1]); // 获取查询参数
      const det = query.get('det'); 
      const id = parseInt('id')

      let responseData;
      switch (det) {
        case 'man':
          responseData = {
            code: 0,
            msg: 'ok',
            data: {
              id:1,
              detailsurls:['https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg','https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg'],
              introduce:'35周岁以上的男性壮年，为相关科室诊断提供重要依据',
              rules:['1、请至少提前一天预定','2、线上预约完成后，体检日拿身份证到医院前台等级即可开始体检','3、医院提供营养早餐一份','4、体检者可或免费停车票']
            }
          };
          break;
          case 'woman':
          responseData = {
            code: 0,
            msg: 'ok',
            data: {
              id:1,
              detailsurls:['https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg','https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg'],
              introduce:'35周岁以上的女性壮年，为相关科室诊断提供重要依据',
              rules:['1、请至少提前一天预定','2、线上预约完成后，体检日拿身份证到医院前台等级即可开始体检','3、医院提供营养早餐一份','4、体检者可或免费停车票']
            }
          };
          break;
          case 'parent':
            responseData = {
              code: 0,
              msg: 'ok',
              data: {
                id:1,
                detailsurls:['https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg','https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg'],
                introduce:'60周岁以上的男性老年人，为相关科室诊断提供重要依据',
                rules:['1、请至少提前一天预定','2、线上预约完成后，体检日拿身份证到医院前台等级即可开始体检','3、医院提供营养早餐一份','4、体检者可或免费停车票']
              }
            };
            break;
            case 'work':
              responseData = {
                code: 0,
                msg: 'ok',
                data: {
                  id:1,
                  detailsurls:['https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg','https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg'],
                  introduce:'即将入职或已入职的广大员工，为相关科室诊断提供重要依据',
                  rules:['1、请至少提前一天预定','2、线上预约完成后，体检日拿身份证到医院前台等级即可开始体检','3、医院提供营养早餐一份','4、体检者可或免费停车票']
                }
          };
          break;
          default:
            responseData = {
              code: -1,
              msg: '请传递正确的det'
            };
          }

      

      return responseData;
    }
  },
  // 套餐详情的体检项目
  {
    url: '/api/physical/experienceProject',
    method: 'get',
    response: (req) => {
      const query = new URLSearchParams(req.url.split('?')[1]); // 获取查询参数
      const det = query.get('det'); 
      const id = parseInt('id')

      let responseData;
      switch (det) {
        case 'man':
          responseData = {
            code: 0,
            msg: 'ok',
            data: [
              {
                title:'一般情况',
                range:'身高、体重、血压、脉搏',
                significance:'了解身体一般状况'
              },
              {
                title:'内科',
                range:'胸部(心脏、双肺、胸腔) 、腹部',
                significance:'了解内科一般状况'
              },
              {
                title:'体成分测定',
                range:'体脂肪测定',
                significance:'体脂肪率脂肪占体重的百分比，以此判断超重或肥胖'
              },
              {
                title:'眼科常规检查',
                range:'视力',
                significance:'进行视力、辨色力检查'
              },
            ]
          };
          break;
          case 'woman':
          responseData = {
            code: 0,
            msg: 'ok',
            data: [
              {
                title:'一般情况',
                range:'身高、体重、血压、脉搏',
                significance:'了解身体一般状况'
              },
              {
                title:'内科',
                range:'胸部(心脏、双肺、胸腔) 、腹部',
                significance:'了解内科一般状况'
              },
              {
                title:'体成分测定',
                range:'体脂肪测定',
                significance:'体脂肪率脂肪占体重的百分比，以此判断超重或肥胖'
              },
              {
                title:'眼科常规检查',
                range:'视力',
                significance:'进行视力、辨色力检查'
              },
            ]
          };
          break;
          case 'parent':
            responseData = {
              code: 0,
              msg: 'ok',
              data: [
                {
                  title:'一般情况',
                  range:'身高、体重、血压、脉搏',
                  significance:'了解身体一般状况'
                },
                {
                  title:'内科',
                  range:'胸部(心脏、双肺、胸腔) 、腹部',
                  significance:'了解内科一般状况'
                },
                {
                  title:'体成分测定',
                  range:'体脂肪测定',
                  significance:'体脂肪率脂肪占体重的百分比，以此判断超重或肥胖'
                },
                {
                  title:'眼科常规检查',
                  range:'视力',
                  significance:'进行视力、辨色力检查'
                },
              ]
            };
            break;
            case 'work':
              responseData = {
                code: 0,
                msg: 'ok',
                data: [
                  {
                    title:'一般情况',
                    range:'身高、体重、血压、脉搏',
                    significance:'了解身体一般状况'
                  },
                  {
                    title:'内科',
                    range:'胸部(心脏、双肺、胸腔) 、腹部',
                    significance:'了解内科一般状况'
                  },
                  {
                    title:'体成分测定',
                    range:'体脂肪测定',
                    significance:'体脂肪率脂肪占体重的百分比，以此判断超重或肥胖'
                  },
                  {
                    title:'眼科常规检查',
                    range:'视力',
                    significance:'进行视力、辨色力检查'
                  },
                ]
          };
          break;
          default:
            responseData = {
              code: -1,
              msg: '请传递正确的det'
            };
          }
      return responseData;
    }
  },
  // 体检加项
  {
    url: '/api/physical/projectAddition',
    method: 'get',
    response: (req) => {
      const query = new URLSearchParams(req.url.split('?')[1]); // 获取查询参数
      const det = query.get('det'); 
      const id = parseInt('id')

      let responseData;
      switch (det) {
        case 'man':
          responseData = {
            code: 0,
            msg: 'ok',
            data: [{
              title:'肝功三项',
              oldPrice:80,
              sellingprice:30,
              volume:12345
            },
            {
              title:'肾功三项',
              oldPrice:35,
              sellingprice:25,
              volume:13541
            },
            {
              title:'脑部CT',
              oldPrice:600,
              sellingprice:300,
              volume:3546
            },
            {
              title:'心电图',
              oldPrice:180,
              sellingprice:99,
              volume:8612
            }
            ]
          };
          break;
          case 'woman':
          responseData = {
            code: 0,
            msg: 'ok',
            data: [{
              title:'肝功三项',
              oldPrice:80,
              sellingprice:30,
              volume:12345
            },
            {
              title:'肾功三项',
              oldPrice:35,
              sellingprice:25,
              volume:13541
            },
            {
              title:'脑部CT',
              oldPrice:600,
              sellingprice:300,
              volume:3546
            },
            {
              title:'心电图',
              oldPrice:180,
              sellingprice:99,
              volume:8612
            }
            ]
          };
          break;
          case 'parent':
            responseData = {
              code: 0,
              msg: 'ok',
              data: [{
                title:'肝功三项',
                oldPrice:80,
                sellingprice:30,
                volume:12345
              },
              {
                title:'心脏超声',
                oldPrice:710,
                sellingprice:599,
                volume:17641
              },
              {
                title:'肝胆胰脾双肾超声',
                oldPrice:860,
                sellingprice:499,
                volume:9428
              },
              {
                title:'脑部CT',
                oldPrice:600,
                sellingprice:300,
                volume:3546
              },
              {
                title:'心电图',
                oldPrice:180,
                sellingprice:99,
                volume:8612
              }
              ]
            };
            break;            
          default:
            responseData = {
              code: -1,
              msg: '请传递正确的det'
            };
          }
      return responseData;
    }
  },
  // 获取适用机构列表
  {
    url:'/api/physical/organList',
    method:'get',
    response:()=>{
      return{
        code:0,
        msg:'ok',
        data:[
          {
            organId:10001,
            imgurl:'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
            name:'南方医科大学附属第三医院',
            PeopleNum:643,
            distance:4.5,
          },
          {
            organId:10002,
            imgurl:'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
            name:'慈铭体检',
            PeopleNum:912,
            distance:9.0,
          }
        ]
      }
    }
  }
];




// 套餐列表调用的函数
function generateData(totalCount, name, location, basePrice, priceDifference, page, pageSize) {
  const data = [];

  const startIndex = (page - 1) * pageSize;
  const endIndex = Math.min(startIndex + pageSize, totalCount);

  for (let i = startIndex; i < endIndex; i++) {
    const randomPrice = Math.floor(Math.random() * Math.abs(priceDifference)) + 1; // 生成1到priceDifference之间的随机数
    const oldPrice = basePrice + randomPrice; // oldprice为basePrice加上随机数
    const sellingPrice = oldPrice - priceDifference; // sellingprice为oldprice减去priceDifference
    const id = i + 1; // 生成唯一标识符
    data.push({
      id: id,
      imgurl: 'https://fastly.jsdelivr.net/npm/@vant/assets/cat.jpeg',
      name: name,
      location: location,
      oldprice: oldPrice,
      sellingprice: sellingPrice
    });
  }
  return data;
}