import { mock } from 'mockjs'


export default [
  // 获取预约首页数据
  {
    url: '/api/subscrible/getSubchild',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        ...mock({
          ['data|4']: [{
            'id|+1': 0,
            name: '@cname',
            'tel|+10086': 13812345678,
            'state|+1': 0,
            institution: '【慈铭】广州体育中心体检中心',
            time: '@date',
            'number|+1': 1001
          }]
        })
      }
    }
  },
  {
    url: '/api/subscrible/getData:id',
    method: 'get',
    response: (query) => {
      let ids=query.url[query.url.length - 1]
      return {
        code: 0,
        msg: 'ok',
        data: {
          ...mock({
            item: {
              id: ids,
              name: '@cname',
              'tel|+10086': 13812345678,
              state: ids,
              institution: '【慈铭】广州体育中心体检中心',
              time: '@date',
              'number|+1': 1001
            },
            data: {
              OdetailID: /\d{7}/,
              offerName: '老年人体检套餐豪华版',
              offerPrice: 998,
              ['Oproject|2-4']: [{
                'OprojectName|+1': ['脑部CT', '心电图', '胸部X光', '血尿酸'],
                'Oprice|20-500': 1000,
              }],
              'OtotalPrice|1000-3000': 1000,
              AdetailID: /\d{7}/,
              ['Aproject|1-3']: [{
                'AprojectName|+1': ['肝功三项', '肾功三项', '乙肝病毒检查'],
                'Aprice|20-500': 500,
              }],
              'AtotalPrice|100-1000': 1000,
              refund: ids,
            }
          })
        }
      }
    }
  },
  // 获取订单数据
  {
    url: '/api/subscrible/getSubDetailchild',
    method: 'get',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: {
          ...mock({
            OdetailID: /\d{7}/,
            offerName: '老年人体检套餐豪华版',
            offerPrice: 998,
            ['Oproject|2-4']: [{
              'OprojectName|+1': ['脑部CT', '心电图', '胸部X光', '血尿酸'],
              'Oprice|20-500': 1000,
            }],
            'OtotalPrice|1000-3000': 1000,
            AdetailID: /\d{7}/,
            ['Aproject|1-3']: [{
              'AprojectName|+1': ['肝功三项', '肾功三项', '乙肝病毒检查'],
              'Aprice|20-500': 500,
            }],
            'AtotalPrice|100-1000': 1000,
            'refund|1': [0, 1, 2, 3],
          })
        }
      }
    }
  },
  // 取消订单修改数据
  {
    url: '/api/subscrible/editSubchildData',
    method: 'post',
    response: () => {
      return {
        code: 0,
        msg: 'ok',
        data: 1
      }
    }
  }
]