import Mock from "mockjs";

// mock 用户数据
export default [
    // 用户登录
    {
        url: '/api/user/login',
        method: 'post',
        response: () => {
            return {
                code: 0,
                msg: 'ok',
                data: {
                    userid: 1,
                    name: 'cunFox',
                    token: 'rtreghdhdgfhgdfhgdfhdhdfggdgfdhtjkbncvx',
                }
            }
        }
    },
    // 退出登录
    {
        url: '/api/user/logout',
        method: 'get',
        response: () => {
            return {
                code: 0,
                msg: 'ok',
                data: 1
            }
        }
    },
    // 获取机构列表
    {
        url: '/api/user/organList',
        method: 'get',
        response: ({ query }) => {
            let id = query.id
            return {
                code: 0,
                msg: 'ok',
                data: {
                    id,
                    ...Mock.mock({
                        'organData|3': [
                            {
                                "organId|+1": 100001,
                                "organName|1": ["南方医科大学附属第三医院", "南医三院", "美年大健康", "慈铭体检"],
                                'combo|50-200': 100,
                                'url|+1': [
                                    'https://img95.699pic.com/photo/50118/7073.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/xsj/0p/7a/8w.jpg!/fh/300',
                                    'https://img95.699pic.com/xsj/0m/5a/0g.jpg!/fh/300',
                                ]
                            }
                        ]
                    })
                }
            }
        }
    },
     // 获取套餐列表
     {
        url: '/api/user/mealList',
        method: 'get',
        response: ({ query }) => {
            let id = query.id
            return {
                code: 0,
                msg: 'ok',
                data: {
                    id,
                    ...Mock.mock({
                        'mealData|8': [
                            {
                                "organId|+1": 100001,
                                "mealId|+1": 10000001,
                                "mealName|1":['老年人体检套餐（女）','老年人体检套餐（男）','幼儿养护'],
                                "organName|1": ["南方医科大学附属第三医院", "南医三院", "美年大健康", "慈铭体检"],
                                oidMoney: 1200,
                                'discount|100-300': 100,
                                'url|+1': [
                                    'https://img95.699pic.com/photo/50121/4315.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50071/6778.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50165/7623.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50249/6482.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50071/6781.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50074/4653.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50118/7084.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50121/4320.jpg_wh300.jpg!/fh/300/quality/90',
                                    
                                ]
                            }
                        ]
                    })
                }
            }
        }
    },
]
