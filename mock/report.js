import Mock from "mockjs";

export default [
    {
        url: '/api/report/reportInfo',
        method: 'post',
        response: () => {
            return {
                code: 0,
                msg: 'ok',
                data: {
                    ...Mock.mock({
                        'reportList|15': [
                            {
                                'reportId|+1': 1000001,
                                "organName|1": ["南方医科大学附属第三医院", "南医三院", "美年大健康", "慈铭体检"],
                                username: 'cunfox',
                                tel: '15809282910',
                                'date': '@DATE',
                            }
                        ]

                    })
                }
            }
        }
    },
    {
        url: '/api/report/reportList',
        method: 'get',
        response: ({ query }) => {
            let organId = query.organId || 100001
            return {
                code: 0,
                msg: 'ok',
                data: {
                    ...Mock.mock({
                        'mealData|4': [
                            {
                                organId,
                                "mealId|+1": 10000001,
                                "mealName|1": ['老年人体检套餐（女）', '老年人体检套餐（男）', '幼儿养护'],
                                "organName|1": ["南方医科大学附属第三医院", "南医三院", "美年大健康", "慈铭体检"],
                                oidMoney: 1200,
                                'discount|100-300': 100,
                                'url|+1': [
                                    'https://img95.699pic.com/photo/50121/4315.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50071/6778.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50165/7623.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50249/6482.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50071/6781.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50074/4653.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50118/7084.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/photo/50121/4320.jpg_wh300.jpg!/fh/300/quality/90',

                                ]
                            }
                        ]
                    })
                }
            }
        }
    },
    {
        url: '/api/report/reportItem',
        method: 'get',
        response: ({ query }) => {
            let cid = query.cid || 1001
            return {
                code: 0,
                msg: 'ok',
                data: {
                    itemInfo: {
                        cid: cid,
                        cName: '丙氨酸氨基转氨酶',
                        sName: "ALT",
                        scope: '0-41',
                        unit: 'U/L',
                        "hint|1": ['提示1', "提示2", '提示3']
                    }
                }
            }
        }
    },
    {
        url: '/api/report/reportItemInfo',
        method: 'get',
        response: ({ query }) => {
            let reportId = query.reportId
            return {
                code: 0,
                msg: 'ok',
                data: {
                    ...Mock.mock({
                        reportInfo: {
                            reportId: reportId,
                            'health|60-100': 98,
                            history: [
                                {
                                    cid: 1001,
                                    title: '身高',
                                    cont: '160.5cm'
                                },
                                {
                                    cid: 1002,
                                    title: '体重指数BMI',
                                    cont: '18.3'
                                },
                                {
                                    cid: 1003,
                                    title: '收缩压',
                                    cont: '96mmHg'
                                },
                                {
                                    cid: 1004,
                                    title: '舒张压',
                                    cont: '66mmHg'
                                },
                                {
                                    cid: 1005,
                                    title: '血小板',
                                    cont: '99.00*10^9/L'
                                },
                            ],
                            'abnormal|1-5': [
                                {
                                    'cid|+1': 1001,
                                    'title|+1': ['身高', '体重指数BMI', '收缩压', '舒张压', '血小板'],
                                    'cont|+1': ['160.5cm', "20.3", '105mmHg', '102mmHg', '103.00*10^9/L']
                                }
                            ],
                            conclusion: [
                                {
                                    cid: 1005,
                                    content: "血液常规提示：血小板值偏低[103.00*10^9/L]。建议内科复查随访"
                                }
                            ],
                            hygiene: [
                                {
                                    cid: 1005,
                                    content: '血小板减少：血小板是血液基本成分之一，参与止血和血栓形成，血小板计数是每单位容积周围血液中血小板的数量，低于100*10（9次方）/L为血小板计数减少。清晨、女性月经前可呈一过性减少，一般无临床意义。病理性减少见于原发性血小板减少性紫癜、再生障碍性贫血、放射性损伤、某些药物反应、脾功能亢进等。 建议您复查血常规、内科随访。'
                                }
                            ]
                        }


                    })
                }
            }
        }
    },
    {
        url: '/api/report/allObject',
        method: 'get',
        response: ({ query }) => {
            let reportId = query.reportId
            return {
                code: 0,
                msg: 'ok',
                data: {
                    ...Mock.mock({
                        reportInfo: {
                            reportId: reportId,
                            common: [
                                {
                                    cid: 1001,
                                    title: '身高',
                                    cont: '160.5cm',
                                    switch: 0
                                },
                                {
                                    cid: 1002,
                                    title: '体重指数BMI',
                                    cont: '18.3',
                                    switch: 0
                                },
                                {
                                    cid: 1003,
                                    title: '收缩压',
                                    cont: '96mmHg',
                                    switch: 0
                                },
                                {
                                    cid: 1004,
                                    title: '舒张压',
                                    cont: '66mmHg',
                                    switch: 0
                                },
                                {
                                    cid: 1006,
                                    title: '脉搏',
                                    cont: '79次/分',
                                    switch: 0
                                },
                            ],
                            commonInfo: '未见明显异常',
                            blood: [
                                {
                                    cid: 1005,
                                    title: '血小板',
                                    cont: '103.00*10^9/L',
                                    switch: 1
                                },
                                {
                                    cid: 1007,
                                    title: '白细胞',
                                    cont: '4.38*10^9/L',
                                    switch: 0
                                },
                                {
                                    cid: 1008,
                                    title: '红细胞',
                                    cont: '131.0g/L',
                                    switch: 0
                                },
                                {
                                    cid: 1009,
                                    title: '红细胞压积',
                                    cont: '0.21%',
                                    switch: 0
                                }
                            ],
                            bloodInfo: '血小板值偏低[99.00*10^9/L],建议复查随访',
                            liver: [
                                {
                                    cid: 1010,
                                    title: '内氨酸氨基转氨酶',
                                    cont: '14U/L',
                                    switch: 0
                                },
                                {
                                    cid: 1011,
                                    title: '天门冬氨酸氨基转氨酶',
                                    cont: '22U/L',
                                    switch: 0
                                },
                                {
                                    cid: 1012,
                                    title: 'AST/ALT',
                                    cont: '1.57',
                                    switch: 0
                                },
                                {
                                    cid: 1010,
                                    title: 'r-谷氨酷转移酶',
                                    cont: '14U/L',
                                    switch: 0
                                },
                            ],
                            liverInfo: '未见明显异常',
                            cUltrasound: [
                                {
                                    uid: 101,
                                    title: '甲状腺彩超',
                                    switch: 0
                                },
                                {
                                    uid: 101,
                                    title: '肝脏彩超',
                                    switch: 0
                                },
                                {
                                    uid: 101,
                                    title: '盆腔彩超',
                                    switch: 0
                                },
                                {
                                    uid: 101,
                                    title: '乳腺彩超',
                                    switch: 0
                                }
                            ],
                            cUltrasoundInfo:'正常，肝脏下形态大小正常'
                        }


                    })
                }
            }
        }
    }
]