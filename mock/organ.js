import Mock from "mockjs";

export default [
    {
        url: '/api/organ/info',
        method: 'get',
        response: ({ query }) => {
            let organId = query.organId || 10001
            return {
                code: 0,
                msg: 'ok',
                data: {
                    ...Mock.mock({
                        organData: {
                            organId: organId,
                            "organName|1": ["南方医科大学附属第三医院", "南医三院", "美年大健康", "慈铭体检"],
                            'usersOrder|50-100': 60,
                            'urlDistance|1-300000': 2,
                            day: '周一至周六',
                            date: '8：30-16：30',
                            tel: '020-83761254',
                            url: "广州市天河区中山大道西183号新门诊3楼体检中心",
                            'combo|50-200': 100,
                            'urlImg|1': [
                                'https://img95.699pic.com/photo/50118/7073.jpg_wh300.jpg!/fh/300/quality/90',
                                'https://img95.699pic.com/xsj/0p/7a/8w.jpg!/fh/300',
                                'https://img95.699pic.com/xsj/0m/5a/0g.jpg!/fh/300',
                            ]
                        }

                    })
                }
            }
        }
    },
    {
        url: '/api/organ/list',
        method: 'get',
        response: () => {
            return {
                code: 0,
                msg: 'ok',
                data: {
                    ...Mock.mock({
                        'organList|20': [
                            {
                                'organId|+1': 10001,
                                "organName|1": ["南方医科大学附属第三医院", "南医三院", "美年大健康", "慈铭体检"],
                                'usersOrder|50-100': 60,
                                'urlDistance|1-300000': 2,
                                day: '周一至周六',
                                date: '8：30-16：30',
                                tel: '020-83761254',
                                url: "广州市天河区中山大道西183号新门诊3楼体检中心",
                                'combo|50-200': 100,
                                'urlImg|1': [
                                    'https://img95.699pic.com/photo/50118/7073.jpg_wh300.jpg!/fh/300/quality/90',
                                    'https://img95.699pic.com/xsj/0p/7a/8w.jpg!/fh/300',
                                    'https://img95.699pic.com/xsj/0m/5a/0g.jpg!/fh/300',
                                ]
                            }
    
                        ]

                    }),
                    total:20
                }
            }
        }
    }
]