import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import persistedState from 'pinia-plugin-persistedstate'
import registerComponents  from './components'
import App from './App.vue'
import router from './router'
import { Lazyload } from 'vant'
import { Dialog } from 'vant'
import { ShareSheet } from 'vant';
import 'amfe-flexible'
import 'vant/lib/index.css';
const app = createApp(App)

const pinia = createPinia()
pinia.use(persistedState)
app.use(ShareSheet);
app.use(pinia)
app.use(router)
app.use(Lazyload, {
    lazyComponent: true,
  })
app.use(Dialog);
registerComponents(app)
app.mount('#app')
