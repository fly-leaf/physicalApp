import { post, get,put } from '@/utils/http'

/**
 * 获取机构数据
 * @param {*} organId 机构id
 * @returns object
 */
export const itemOrganInfoApi = (organId=10001) => get(`/api/organ/info?organId=${organId}`)

/**
 * 获取机构列表数据
 * @returns array
 */
export const itemOrganListApi = () => get(`/api/organ/List`)