import { post, get,put } from '@/utils/http'

/**
 * 获取报告列表
 * @param {*} userId 用户id
 * @returns array
 */
export const reportInfoListApi = (userId) => post('/api/report/reportInfo',userId)  

/**
 * 获取机构主页中推荐的报告列表
 * @param {*} organId 机构id
 * @returns array
 */
export const reportListApi = (organId) => get(`/api/report/reportList?organId=${organId}`)  

/**
 * 获取报告详情需要展示的数据
 * @param {*} reportId 报告id
 * @returns object
 */
export const reportInfoApi = (reportId) => get(`/api/report/reportItemInfo?reportId=${reportId}`)  

/**
 * 报告中的单个项目数据
 * @param {*} cid 单个项目id
 * @returns object
 */
export const reportItemApi = (cid) => get(`/api/report/reportItem?cid=${cid}`)  

/**
 * 报告中的所有项目数据
 * @param {*} reportId 报告id
 * @returns object
 */
export const reportAllObjectApi = (reportId) => get(`/api/report/allObject?reportId=${reportId}`)  