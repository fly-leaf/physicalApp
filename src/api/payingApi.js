import { post, get,put } from '@/utils/http'



/**
 *  向数据库中添加订单
 *  @returns Promise
 */
export const payingAddOrderApi = (data)=>post('/api/paying/addOrder',data)



/**
 *  生成订单号 
 *  @returns Promise
 */
export const payingGenerateOrderIdApi = (token,checkList)=>post('/api/paying/generateOrderId',{token:token,checkList:checkList})



/**
 *  获得价格 
 *  @returns Promise
 */
export const payingGetPriceApi = (orderId)=>post('/api/paying/getPrice',orderId)




/**
 *  返回体检人信息 
 *  @returns Promise
 */
export const getPhysicalManApi = (token)=>post('/api/paying/selectPhysicalMan',token)


/**
 *  创建体检人信息（提交表单）
 *  @returns Promise
 */
export const addPhysicalManApi = (data)=>post('/api/paying/addPhysicalMan',data)


/**
 *  提交体检人
 *  @returns Promise
 */
export const submitPhysicalManApi = (data)=>post('/api/paying/submitPhysicalMan',data)



/**
 *  获取订单信息
 *  @returns Promise
 */
export const getOrderApi = (orderId)=>post('/api/paying/getOrderInformation',orderId)
