import { post, get,put } from '@/utils/http'


/**
 * 
 * @returns 预约的数据
 */
export const getSubchildDataApi = ()=>get('/api/subscrible/getSubchild')



/**
 * 
 * @returns 预约的数据
 */
export const getDataApi = (id)=>get(`/api/subscrible/getData${id}`)
/**
 * 
 * @returns 预约的套餐数据
 */
export const getSubDetailchildDataApi = ()=>get('/api/subscrible/getSubDetailchild')


/**
 * 
 * @param { } data 要修改数据
 * @returns 成功与否
 */
export const editSubchildDataApi = (data)=>post('/api/subscrible/editSubchildData',data)