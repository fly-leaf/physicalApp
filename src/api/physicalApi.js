import { post, get,put } from '@/utils/http'



/**
 *  套餐分类 
 *  @returns Promise
 */
export const physicalPackageClassification = ()=>get('/api/physical/classification')



/**
 *  套餐列表 
 *  @returns Promise
 */
export const physicalPackageList = (type,page=1)=>get(`/api/physical/PackageList?det=${type}&page=${page}`)


/**
 *  套餐详情的上半部分
 *  @returns Promise
 */
export const physicalPackageDetailsApi = (id,type)=>get(`/api/physical/packageDetails?id=${id}&det=${type}`)


/**
 *  套餐详情的图文详情
 *  @returns Promise
 */
export const physicalPackagePictureTextDetailApi = (id,type)=>get(`/api/physical/pictureTextDetails?id=${id}&det=${type}`)



/**
 *  套餐详情的体检项目
 *  @returns Promise
 */
export const physicalPackageExperienceProjectApi = (id,type)=>get(`/api/physical/experienceProject?id=${id}&det=${type}`)



/**
 *  体检加项
 *  @returns Promise
 */
export const physicalPackageAditionApi = (id,type)=>get(`/api/physical/projectAddition?id=${id}&det=${type}`)




/**
 *  获取适用机构列表
 *  @returns Promise
 */
export const getOrganListApi = ()=>get('/api/physical/organList')


