import { post, get,put } from '@/utils/http'


/**
 *  用户登录 
 *  @param {object}  userData 用户登录表单数据
 *  @returns data
 */
export const userLoginApi = (userData)=>post('/api/user/login',userData)


/**
 * 用户退出登录
 * @returns data
 */
export const userLogoutApi = ()=>get('/api/user/logout')


/**
 * 获取用户个人的推荐机构
 * @param {*} id 用户个人id
 * @returns  data
 */
export const userOrganListApi = (id=-1)=>get(`/api/user/organList?id=${id}`)

/**
 * 获取用户个人的推荐套餐
 * @param {*} id 用户个人id
 * @returns data
 */
export const userMealListApi = (id=-1)=>get(`/api/user/mealList?id=${id}`)

