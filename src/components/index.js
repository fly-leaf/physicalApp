import selectBox from './selectBox/index.js'
import organBox from './organBox/index.js'
import setMealBox from './setMealBox/index.js'
import situationTitle from './situationTitle'
import reportBox from './reportBox'

export default (app) => {
  app.component('SelectBox', selectBox)
  app.component('OrganBox', organBox)
  app.component('SetMealBox', setMealBox)
  app.component('situationTitle', situationTitle)
  app.component('ReportBox', reportBox)
}