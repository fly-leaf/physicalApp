import { ref, onMounted } from 'vue'
import { getDataApi } from '@/api/subscribeApi'

// 获取数据并返回一个ref对象
export const useGetData = (ids) => {
  let item = ref()
  let data = ref()
  const loadData = async (ids) => {
    let ret = await getDataApi(ids)
    if (ret.code === 0) {
      data.value = ret.data.data
      item.value = ret.data.item
    }
  }
  onMounted(() => {
    loadData(ids)
  })
  return [item,data,loadData]
}
