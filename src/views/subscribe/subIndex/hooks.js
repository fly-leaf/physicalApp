import { ref, onMounted } from 'vue'
import { getSubchildDataApi } from '@/api/subscribeApi'

// 获取“我的预约”首页数据并返回一个ref对象
export const useGetSubchildData = () => {
  let data = ref()
  const loadData = async () => {
    let ret = await getSubchildDataApi()
    if (ret.code === 0) {
      data.value = ret.data
    }
  }
  onMounted(() => {
    loadData()
  })
  return data
}
