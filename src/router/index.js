import { createRouter, createWebHistory } from 'vue-router'
import checkUserLogin from './hooks/checkUserLogin'
import physicalChildren from './routers/physical'
import organChildren from './routers/organ'
import reportChildren from './routers/reportInfo'
import subRoutes from './routers/subscribe'
import centerChildren from './routers/userCenter'
 
const router = createRouter({
  history: createWebHistory('/'),
  routes: [
    // 首页
    {
      path: '/',
      name: 'admin',
      component: ()=>import('@/views/admin/index.vue'),
      // children
    },
    // 登录
    {
      path: '/login',
      component: () => import('@/views/login/index.vue'),
      meta: {
        nologin: true,
        menu: ''
      }
    },
    // 体检
    {
      path: '/physical',
      name: 'physical',
      redirect:'/physical/packageClassification',
      children:physicalChildren
    },
    // 预约
    {
      path: '/subscribe',
      component: () => import('@/views/subscribe/index.vue'),
      name: '我的预约',
      redirect:'/subscribe/subIndex',
      children: subRoutes
    },
    // 报告
    {
      path: '/report',
      redirect: '/report/reportList',
      name: 'report',
      children:reportChildren
    },
    // 解读
    {
      path: '/read',
      component: () => import('@/views/read/index.vue'),
      name: 'read',
    },
    // 个人中心
    {
      path: '/user',
      redirect: '/user/userCenter',
      name: '个人中心',
      children:centerChildren
    },
    // 机构
    {
      path: '/organ',
      redirect: '/organ/organInfo',
      children:organChildren,
      name:'organ'
    }
  ]
})
router.beforeEach(checkUserLogin)

export default router
