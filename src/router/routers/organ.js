export default [
    {
        path: 'organInfo',
        name: '机构主页',
        component: () => import('@/views/organ/organInfo/index.vue'),
    },
    {
        path: 'organList',
        name: '体检机构',
        component: () => import('@/views/organ/organList/index.vue'),
    },

]