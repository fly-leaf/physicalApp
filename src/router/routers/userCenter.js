export default [
    {
        path: 'userCenter',
        component: () => import('@/views/userCenter/index.vue'),
        name: '个人中心'
    },
    {
        path: 'userSafety',
        component: () => import('@/views/userCenter/userSafety/index.vue'),
        name: '账户与安全'
    },
    {
        path: 'userFamily',
        component: () => import('@/views/userCenter/userFamily/index.vue'),
        name: '关于我们'
    },
    {
        path: 'userWallet',
        name: '钱包',
        component: () => import('@/views/userCenter/userWallet/index.vue')
    },
]