 // 体检
const routes = [
            {
                path: 'packageClassification',
                name: '套餐分类',
                component: () => import('@/views/physical/index.vue'),

            },
            {
                path:'packageList',
                name:'体检套餐',
                component:()=>import('@/views/packageList/index.vue'),
            },
            {
                path:'packageDetails/:id/:det',
                name:'套餐详情',
                component:()=>import('@/views/packageDetails/index.vue'),
            },
            {
                path:'packageAddition/:id/:det',
                name:'体检加项',
                component:()=>import('@/views/packageAddition/index.vue'),
            },
            {
                path:'packagePaying/:orderId/:name?/:du?',
                name:'支付完成预约',
                component:()=>import('@/views/paying/index.vue')
            },
            {
                path:'choosePhysicalMan/:orderId',
                name:'选择体检人',
                component:()=>import('@/views/choosePhysicalMan/index.vue')
            },
            {
                path:'addPhysicalMan',
                name:'添加体检人',
                component:()=>import('@/views/addPhysicalMan/index.vue')
            },
        ]
            // {
            //     path: 'add',
            //     name: 'PhysicalAdd',
            //     component: () => import('@/views/physical/add'),
            //     meta: {
            //     }
            // },
            // {
            //     path: 'edit/:id',
            //     name: 'PhysicalEdit',
            //     component: () => import('@/views/physical/edit'),

export default routes