
// 报告
export default [
  {
    path: 'reportInfo',
    name: '报告详情',
    component: () => import('@/views/reportInfo/info/index.vue'),
  },
  {
    path: 'reportList',
    component: () => import('@/views/reportInfo/index.vue'),
    name: '体检报告',
  },
  {
    path: 'allObject',
    component: () => import('@/views/reportInfo/allObject/index.vue'),
    name: '全部项目',
  },
  {
    path: 'reportItem',
    component: () => import('@/views/reportInfo/itemObject/index.vue'),
    name: 'reportItem',
  },
]