// 预约
const subRoutes = [
  {
    path: '/subscribe/subIndex',
    name: '我的预约',
    component: () => import('@/views/subscribe/subIndex/index.vue'),
  },
  {
    path: '/subscribe/subDetail',
    name: '体检预约详情',
    component: () => import('@/views/subscribe/subDetail/index.vue'),
    query:{}
  },
  {
    path: '/subscribe/subRemove',
    name: '取消预约并退款',
    component: () => import('@/views/subscribe/subRemove/index.vue'),
    query:{}
  }
]
export default subRoutes