import axios from 'axios'

const ins = axios.create({
  timeout: 10000,
  baseURL: ''
})

// 响应拦截器
ins.interceptors.response.use(res => {
  return res.data
}, err => {
  return Promise.reject(err)
})

/**
 * Post请求
 * @param url 请求的url地址
 * @param data 请求体数据
 * @param config 请求配置 -- 额外请求
 * @returns Promise
 */
export const post = (url, data, config = {}) => ins.post(url, data, config)

/**
 * Put请求
 * @param url 请求的url地址
 * @param data 请求体数据
 * @param config 请求配置 -- 额外请求
 * @returns Promise
 */
export const put = (url, data, config = {}) => ins.put(url, data, config)


/**
 * Get请求
 * @param url 请求地址
 * @param config 请求配置 -- 额外请求
 * @returns Promise
 */
export const get = (url, config = {}) => ins.get(url, config)