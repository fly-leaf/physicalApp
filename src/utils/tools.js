import dayjs from "dayjs";
import CryptoJS from 'crypto-js'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)

// 距离判断
export const getDistance = (num) => {
  let n = Number(num)
  if (n > 0 && n < 1000) {
    return n + 'm'
  } else if (n => 1000 && n < 500000) {
    let sum = n % 1000
    return '约' + sum + 'km'
  } else {
    return '超出范围'
  }
}

// 延时器
export const delay = (ms = 300) => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

// 计算多当天到向后6天的日期写一个函数  dayjs momentjs
export const getRangeDay = (day = 6) => {
  let days = [];
  let weeks = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
  for (let i = 1; i <= day; i++) {
    let week = "";
    let weekIndex = dayjs().add(i, "day").format("d");
    if (i == 1) {
      week = "明天";
    } else if (i == 2) {
      week = "后天";
    } else {
      week = weeks[weekIndex];
    }
    let daytime = dayjs().add(i, "day");
    let daystr = week + daytime.format("MM月DD日");
    days.push({ daytime: daytime.unix(), daystr });
  }
  return days;
};

// 指定年份日期
export const getFullYearDay = (
  n = 20,
  type = "year"
) => {
  let timestamp;
  if (type === "year") {
    timestamp = dayjs().add(n, "year");
  } else {
    timestamp = dayjs().add(n, "day");
  }
  return [timestamp.format("YYYY-MM-DD"), timestamp];
};

// 时间处理YYYY-MM-DD
export const getDay = (date) => {
  let newDate = dayjs(date).format("YYYY-MM-DD");
  return newDate;
};

// 时间处理YYYY-MM-DD转UTC
export const getUTC = (date) => {
  let newDate = dayjs.utc(date)
  return newDate
}

// 写一个判断是否是移动端
export const isMobile = () => {
  const userAgentInfo = navigator.userAgent;
  const Agents = ["Android", "iPhone", "SymbianOS", "Windows Phone", "Mobile"];
  return Agents.some((item) => userAgentInfo.includes(item));
};

// 判断是否为空对象
export const isEmptyObj = (obj) => {
  return Object.keys(obj).length === 0 ? true : false;
};

// 返回非空的对象属性
// export const noEmptySearchObject = (obj: any) => {
export const noEmptySearchObject = (obj) => {
  // let ret = {}
  let ret = {};
  for (let key in obj) {
    // 它当前有值
    if (obj[key]) {
      // (ret as any)[key] = obj[key]
      ret[key] = obj[key];
    }
  }
  return isEmptyObj(ret) ? null : ret;
};

// 把对象转成query字符串
export const objToQueryString = (obj) => {
  let ret = "";
  for (let key in obj) {
    ret += `&${key}=${[key]}`;
  }
  return ret;
};


// 节流函数
export const throttle = (fn, delay = 100) => {
  let timer = null
  return function (...args) {
    if (!timer) {
      timer = setTimeout(() => {
        fn.apply(this, args)
        timer = null
      }, delay)
    }
  }
}

// 加密函数
export const encryptObject = (object, key) => {
  const jsonString = JSON.stringify(object);
  // 对jsonString进行加密操作，例如使用AES加密算法
  const encryptedString = CryptoJS.AES.encrypt(jsonString, key).toString();
  return encryptedString;
}

// 解密函数
export const decryptObject = (encryptedString, key) => {
  // 对encryptedString进行解密操作，例如使用AES解密算法  
  const decryptedString = CryptoJS.AES.decrypt(encryptedString, key).toString(CryptoJS.enc.Utf8);
  const object = JSON.parse(decryptedString);
  return object;
}